<?php

if(isset($_SESSION)){

}
/* ----------------------------------------------------------------------------
変数の設定
---------------------------------------------------------------------------- */
$is_error = false; // エラーフラグ
$error_message = array(); // エラーメッセージ配列
$send_contents = array(); // 送信されたデータの保存配列
$is_back = false; // 「戻る」ボタンからの遷移フラグ

// データ操作クラス
$formData = new formData();

/* ----------------------------------------------------------------------------
ページ遷移のチェック
---------------------------------------------------------------------------- */
// submitのPOSTデータ、ワンタイムパスワードのGETデータがない場合
if( !isset($_POST['submit']) && !isset($_SESSION['p']) ) {
    $_SESSION = array();
    //header( 'Location: '. $indexPage );
    //exit;
}

// Submitが押された場合
//if (isset($_POST['submit'])) {
//    unset($_SESSION['back']);
//}

// 戻るが押された場合
if ( isset($_GET['status'])){
    if ( $_GET['status'] == "back"){
        $is_back = true;
    }
}else{
    $is_back = false;
}


/* ----------------------------------------------------------------------------
送信されたデータの解析
---------------------------------------------------------------------------- */
// 戻るが押された場合セッションデータを利用
if ( $is_back ) {
    $post_data = $_SESSION;
} else {
    $post_data = $_POST;
}

// データの解析
$send_contents = $formData->analyzePostData($post_data);

/* ----------------------------------------------------------------------------
送信されたデータのチェック
---------------------------------------------------------------------------- */

// データチェック
$formData->checkPostData( $send_contents, $form_contents );

// チェック済みコンテンツの取得
$contents = $formData->contents;

// エラーフラグの取得
$is_error = $formData->is_error;

// 「戻る」ボタンが押されているかどうか
if ( $is_back ) {
    $is_error = true;
}

// エラーメッセージの取得
$error_message = $formData->error_message;


if($_POST){

    if ( $contents['email'] && $contents['email_confirm']) {
        if( $contents['email'] !== $contents['email_confirm'] ){
            $error_message['email_confirm'] = "同様のメールアドレスを入力してください";
            $is_error = true;
        }
    }

}

// エラーがない場合
if (!$is_error) {
    unset($_POST['submit']);
    $_SESSION = $_POST;
    $_SESSION["p"] = md5( time() + $oneTimePassKey );

    // 確認画面にロケーション
    header ( 'Location: '. $confirmPage );
}





function print_value ($name){
    global $contents;
    if ($contents[$name] != "") {
        echo nl2br(htmlspecialchars($contents[$name], ENT_QUOTES, 'UTF-8'));
    }
}
function print_error_msg ($name){
    global $error_message;
    if ($error_message[$name] != "") {
        echo '<div class="form-err-msg"><strong>' . $error_message[$name] . '</strong></div>';
    }
}
function print_error_head_msg (){
    if ($_SESSION['status'] == "check") {
        echo '<div class="form-alert">';
        echo '<p class="form-alert-msg"><strong>入力内容に誤りがあります。エラーメッセージが出ている項目をご確認下さい。</strong></p>';
        echo '</div><!-- /.form-alert -->';
    }
}
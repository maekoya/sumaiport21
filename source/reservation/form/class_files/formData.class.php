<?php

class formData {

	/**
	 * This file is part of Cypher Point Framework
	 *
	 * Copyright(c) 2007-2008 Cypher point Co.,Ltd. All Rights Reserved.
	 *
	 * http://www.cypher-point.co.jp/
	 */

	var $contents = array();

	// エラーフラグ
	var $is_error;

	// エラーメッセージ保存用配列
	var $error_message = array();

	// checkInputクラス用
	var $checkInput;

	// -------------------------------------------------------------------------------------
	// コンストラクタ
	// -------------------------------------------------------------------------------------
	function formData() {

		// checkInputクラス
		$this->checkInput = new checkInput();

	}

	// -------------------------------------------------------------------------------------
	// POSTデータの解析
	// -------------------------------------------------------------------------------------
	function analyzePostData( $postData ) {

		$count = 0;
		$data = array();

		foreach ($postData as $key => $val) {

			if ( $key != 'submit' && $key != 'p' ) {

				$data[$count]['key'] = $key;
				$data[$count]['val'] = $val;
				$count++;

			}

		}

		return $data;

	}

	// -------------------------------------------------------------------------------------
	// POSTデータのチェック
	// -------------------------------------------------------------------------------------
	function checkPostData( $send_contents, $form_contents ) {

		$is_error = false;

		$data = array();

		for ( $i=0; $i<count($form_contents); $i++ ) {

			$inputType = $form_contents[$i]['type'];
			$key = $form_contents[$i]['name'];
			$label = $form_contents[$i]['label'];
			$check = $form_contents[$i]['check'];

			switch ($inputType) {

				case "checkbox":

					// send_contentsの検索
					$sc_num = 0;
					$val = "";
					$is_no_contents = true;

					for ( $j=0; $j<count($send_contents); $j++ ) {

						if ( $send_contents[$j]['key'] == $key ) {

							$val = $send_contents[$j]['val'];
							$c_array = explode( ",", $check);
							$is_no_contents = false;
							break;

						}

					}

					// チェックボックスにチェックがされていなかった場合
					//if ($is_no_contents) {

					//	$is_error = true;

					//}

					// 配列を代入
					if ( is_array($val) ) {

//						for( $k=0; $k<count($val); $k++  ) {
//
//							// 連想配列のキー
//							$lb = $key.($k+1);
//
//							$data[$lb] = $val[$k];
//
//						}

						$data[$key] = implode("<br />", $val);

					} else {

							$data[$key] = $val;

					}

					// エラーメッセージの初期化
					$error_message[$key] = "";

				break;

				case "radio":

					// send_contentsの検索
					$sc_num = 0;
					$val = "";
					$is_no_contents = true;

					for ( $j=0; $j<count($send_contents); $j++ ) {

						if ( $send_contents[$j]['key'] == $key ) {

							$val = $send_contents[$j]['val'];
							$c_array = explode( ",", $check);
							$is_no_contents = false;
							break;

						}

					}

					// チェックボックスにチェックがされていなかった場合
					if ($is_no_contents) {

						$is_error = true;

					}

					// 配列を代入
					if ( is_array($val) ) {

						for( $k=0; $k<count($val); $k++  ) {

							// 連想配列のキー
							$lb = $key.($k+1);

							$data[$lb] = $val[$k];

						}

					} else {

							$data[$key] = $val;

					}

					// エラーメッセージの初期化
					$error_message[$key] = "";

				break;

				default:

					// send_contentsの検索
					$sc_num = 0;
					$val = "";
					$c_array = array();

					for ( $j=0; $j<count($send_contents); $j++ ) {


						if ( isset($send_contents[$j]['key']) ) {

							if( $send_contents[$j]['key'] == $key) {
								$val = $send_contents[$j]['val'];
								$c_array = explode( ",", $check);
								break;
							}


						} else {

						}

					}

					if (count($c_array)==0) {

						$is_error = true;
					}

					// エラーメッセージの初期化
					$error_message[$key] = "";

					// エラーチェック
					$error_no = 0;
					for ( $l=0; $l<count($c_array); $l++ ) {

						if ( $this->checkError( $c_array[$l], $val ) ) {
							$error_message[$key] = $this->getErrorMessage( $c_array[$l], $label );
							$error_no = $c_array[$l];
							$is_error = true;
							break;
						}

					}

					// 配列を代入
					if ( is_array($val) ) {

						for( $k=0; $k<count($val); $k++  ) {

							// 連想配列のキー
							$lb = $key.($k+1);

							if ( $error_no == 2 || $error_no == 7 ) {
								$data[$lb] = $val[$k];
							} else {
								$data[$lb] = $val[$k];
							}

						}

					} else {

						if ( $error_no == 2 || $error_no == 7 ) {
							$data[$key] = $val;
						} else {
					  	$data[$key] = $val;
						}

					}

				break;

			}

		}

		// 情報を変数に設定
		$this->contents = $data;
		$this->is_error = $is_error;
		$this->error_message = $error_message;

	}

	// -------------------------------------------------------------------------------------
	// POSTデータを整形
	// -------------------------------------------------------------------------------------
	function fixPostData( $post_data ) {

		$data = array();

		for ( $i=0; $i<count($post_data); $i++ ) {

			$key = $post_data[$i]['key'];
			$val = $post_data[$i]['val'];

			// 配列を代入
			if ( is_array($val) ) {

				for( $k=0; $k<count($val); $k++  ) {

					// 連想配列のキー
					$lb = $key.($k+1);

					$data[$lb] = $val[$k];

				}

			} else {

				$data[$key] = $val;

			}

		}

		return $data;

	}


	// -------------------------------------------------------------------------------------
	// エラーチェック
	// -------------------------------------------------------------------------------------
	function checkError( $type, $val ) {

		$is_error = false;

		// 入力必須チェック（空かどうか）
		if ( $type==1 ) {

			$is_error = $this->checkInput->Blank($val);

		}

		// メールアドレスチェック
		if ( $type==2 ) {

			$is_error = $this->checkInput->MailAddress($val);
		}

		// 半角英数チェック
		if ( $type==3 ) {

			$is_error = $this->checkInput->Alnum($val);

		}

		// 半角数字チェック
		if ( $type==4 ) {

			$is_error = $this->checkInput->Num($val);

		}

		// 日付チェック
		if ( $type==5 ) {

			//$is_error = $this->checkInput->Date($val);

		}

		// 郵便番号チェック
		if ( $type==6 ) {

			$is_error = $this->checkInput->ZipCode($val);

		}

		// 電話番号チェック
		if ( $type==7 ) {

			$is_error = $this->checkInput->PhoneNo($val);

		}

		// URLチェック
		if ( $type==8 ) {

			//$is_error = $this->checkInput->URL($val);

		}

		// 都道府県チェック
		if ( $type==9 ) {

			$is_error = $this->checkInput->Prefs($val);

		}

		// カタカナチェック
		if ( $type==10 ) {

			$is_error = $this->checkInput->Kana($val);

		}

		return $is_error;

	}


	// -------------------------------------------------------------------------------------
	// エラーメッセージを取得
	// -------------------------------------------------------------------------------------
	function getErrorMessage( $type, $label ) {

		$e_message = "";

		// 入力必須チェック（空かどうか）
		if ( $type==1 ) {

			$e_message = $label."を入力してください";

		}

		// メールアドレスチェック
		if ( $type==2 ) {

			$e_message = $label."を正しく入力してください";

		}

		// 半角数字チェック
		if ( $type==3 ) {

			$e_message = $label."を半角英数で入力してください";

		}

		// 半角数字チェック
		if ( $type==4 ) {

			$e_message = $label."を半角数字で入力してください";

		}

		// 日付チェック
		if ( $type==5 ) {

			$e_message = $label."を正しく入力してください";

		}

		// 郵便番号チェック
		if ( $type==6 ) {

			$e_message = $label."を正しく入力してください";

		}

		// 電話番号チェック
		if ( $type==7 ) {

			$e_message = $label."を正しく入力してください";

		}

		// URLチェック
		if ( $type==8 ) {

			$e_message = $label."を正しく入力してください";

		}

		// 都道府県チェック
		if ( $type==9 ) {

			$e_message = $label."を正しく入力してください";

		}

		// カタカナチェック
		if ( $type==10 ) {

			$e_message = $label."をカタカナで入力してください";

		}

		return $e_message;

	}

}

?>
<?php
session_start();
session_regenerate_id();
ini_set( 'display_errors', 0 );
/* ----------------------------------------------------------------------------
各種設定
---------------------------------------------------------------------------- */
// ワンタイムパスワード発行用キー
$oneTimePassKey = "4qpqlc9jvLopY0O2x9QieWfnH9";

// SMTPサーバ（お名前.comサーバの場合に必要）
//	$smtpServer = "smtp.domain.tld";

/* ----------------------------------------------------------------------------
ページの設定
---------------------------------------------------------------------------- */
// インデックスページ
$topPage = "/";
// インデックスページ
$indexPage = "/reservation/";
// チェックページ
$checkPage = "/reservation/";
// 確認ページ
$confirmPage = "/reservation/confirm.html";
// 完了ページ
$endPage =  "/reservation/thanks.html";


/* ----------------------------------------------------------------------------
フォームの設定
---------------------------------------------------------------------------- */
// 戻るボタン
$returnBtn = "戻る";

/* ----------------------------------------------------------------------------
メールの設定 - 管理者メール -
---------------------------------------------------------------------------- */
// 宛先
$mailToAdmin = "sapporo-ch@sumai21.net,sapporo-h@sumai21.net";
$mailToCc = "";
$mailToBcc = "wataru@gear8.com";

// 件名
$mailSubjectAdmin = "ご予約フォームからお申し込みを受け付けました";

//本文
$mailBodyAdmin = <<<EOD
ご予約フォームからお申し込みを受け付けました
下記内容を確認の上、担当者は折り返しご連絡ください。

投 稿 者 名 ： {name}
投 稿 時 間 ： {nowDate}
ＩＰアドレス： {ip_address}
ホ ス ト 名 ： {host_name}

--------------------------------------------------
ご入力いただいた情報
受付時間：{nowDate}
--------------------------------------------------

【店舗名】
{shop}
【予約希望日】
第1希望　{year1}年{month1}月{day1}日
第2希望　{year2}年{month2}月{day2}日
第3希望　{year3}年{month3}月{day3}日
【ご相談メニュー】
{menu}
【ご相談内容】
{message}
【すまいポート21をお知りになったキッカケ】
{knowed_full}

【名前】
{name}
【ふりがな】
{kana}
【メールアドレス】
{email}
【ご住所】
{zip}
{address}
【電話番号】
{tel}
【ご家族構成】
{family}人

--------------------------------------------------

このメールに心当たりの無い場合は、お手数ですが
下記連絡先までお問い合わせください。

--------------------------------------------------
すまいポート21　札幌東
〒004-0052
札幌市厚別区厚別中央2条1丁目２-２０
営業時間 AM10:00〜PM7:00（水曜日定休）
TEL／011-887-9191
--------------------------------------------------
すまいポート21　札幌中央
〒064-0953
札幌市中央区宮の森3条1丁目４−１タイびる1F
営業時間 AM10:00〜PM7:00（水曜日定休）
TEL／011-590-0108
--------------------------------------------------

北海道の住まいづくり　すまいポート21
Website ： https://sumai21-hokkaido.com/


EOD;


/* ----------------------------------------------------------------------------
メールの設定 - 訪問者 -
---------------------------------------------------------------------------- */
// 差出人名文字化け対策
mb_language('Japanese');
mb_internal_encoding("UTF-8");

// 送信元
$mailFromLabel = "すまいポート21";	 // FROMの名前
$mailFrom = "sapporo-ch@sumai21.net"; // FROMのメールアドレス
$mailFromUser = mb_encode_mimeheader($mailFromLabel)."<" .$mailFrom.">";

// 件名
$mailSubjectUser = "ご予約フォームからのお申し込みありがとうございます";

// 本文
$mailBodyUser = <<<EOD
{name} 様

この度は、ご予約フォームからお申し込み頂き誠にありがとうございました。
改めて担当者よりご連絡をさせていただきますので、
今しばらくお待ちくださいませ。

--------------------------------------------------
ご入力いただいた情報
受付時間：{nowDate}
--------------------------------------------------

【店舗名】
{shop}
（本メール末尾に店舗情報がございます）
【予約希望日】
第1希望　{year1}年{month1}月{day1}日
第2希望　{year2}年{month2}月{day2}日
第3希望　{year3}年{month3}月{day3}日
【ご相談メニュー】
{menu}
【ご相談内容】
{message}
【すまいポート21をお知りになったキッカケ】
{knowed_full}

【名前】
{name}
【ふりがな】
{kana}
【メールアドレス】
{email}
【ご住所】
{zip}
{address}
【電話番号】
{tel}
【ご家族構成】
{family}人

--------------------------------------------------

このメールに心当たりの無い場合は、お手数ですが
下記連絡先までお問い合わせください。

--------------------------------------------------
すまいポート21　札幌東
〒004-0052
札幌市厚別区厚別中央2条1丁目２-２０
営業時間 AM10:00〜PM7:00（水曜日定休）
TEL／011-887-9191
--------------------------------------------------
すまいポート21　札幌中央
〒064-0953
札幌市中央区宮の森3条1丁目４−１タイびる1F
営業時間 AM10:00〜PM7:00（水曜日定休）
TEL／011-590-0108
--------------------------------------------------

北海道の住まいづくり　すまいポート21
Website ： https://sumai21-hokkaido.com/
EOD;


/* ----------------------------------------------------------------------------
フォームコンテンツの設定
---------------------------------------------------------------------------- */
// $form_contents[] = array( 'label' => '', 'type' => '', 'name' => '', 'check' => '' );
//
// ■ label ： 表示名
//
// ■ type ： inputのタイプ
//
//            text: テキスト
//            radio: ラジオボタン
//            checkbox: チェックボックス
//            select: セレクトボックス
//            hidden: 隠し要素
//
// ■ name  ： inputのname属性に指定する文字列
//
// ■ check ： エラーチェックをする項目の設定。複数設定する場合は「,」区切りで指定する。
//
//             1: 空白チェック
//             2: メールアドレスチェック
//             3: 半角英数チェック
//             4: 半角数字チェック
//             5: 日付チェック ×
//             6: 郵便番号チェック
//             7: 電話番号チェック(044-555-5555(ハイフン入力必須) もしくは [　　　] - [　　　] - [　　　](フォームを3つに分割)
//             8: URLチェック
//             9: 都道府県チェック
//             10: カタカナチェック
//
// ----------------------------------------------------------------------------

// chekcboxを配列で設定すると空白チェック=1が使えないみたいです

$form_contents = array();
$form_contents[] = array( 'type'=>'select',  'label' => '店舗名','name' => 'shop','check' => '1' );

$form_contents[] = array( 'type'=>'select',  'label' => '第1希望（年）','name' => 'year1','check' => '1' );
$form_contents[] = array( 'type'=>'select',  'label' => '第1希望（月）','name' => 'month1','check' => '1' );
$form_contents[] = array( 'type'=>'select',  'label' => '第1希望（日）','name' => 'day1','check' => '1' );
$form_contents[] = array( 'type'=>'select',  'label' => '第2希望（年）','name' => 'year2','check' => '' );
$form_contents[] = array( 'type'=>'select',  'label' => '第2希望（月）','name' => 'month2','check' => '' );
$form_contents[] = array( 'type'=>'select',  'label' => '第2希望（日）','name' => 'day2','check' => '' );
$form_contents[] = array( 'type'=>'select',  'label' => '第3希望（年）','name' => 'year3','check' => '' );
$form_contents[] = array( 'type'=>'select',  'label' => '第3希望（月）','name' => 'month3','check' => '' );
$form_contents[] = array( 'type'=>'select',  'label' => '第3希望（日）','name' => 'day3','check' => '' );

$form_contents[] = array( 'type'=>'checkbox',  'label' => 'ご相談メニュー','name' => 'menu','check' => '' );
$form_contents[] = array( 'type'=>'text',  'label' => 'ご相談内容','name' => 'message','check' => '' );
$form_contents[] = array( 'type'=>'checkbox',  'label' => 'キッカケ','name' => 'knowed','check' => '' );
$form_contents[] = array( 'type'=>'text',  'label' => '媒体名','name' => 'knowed1','check' => '' );
$form_contents[] = array( 'type'=>'text',  'label' => 'その他','name' => 'knowed2','check' => '' );

$form_contents[] = array( 'type'=>'text',  'label' => 'お名前','name' => 'name','check' => '1' );
$form_contents[] = array( 'type'=>'text',  'label' => 'ふりがな','name' => 'kana','check' => '' );
$form_contents[] = array( 'type'=>'text',  'label' => '郵便番号','name' => 'zip','check' => '' );
$form_contents[] = array( 'type'=>'select',  'label' => '住所','name' => 'pref','check' => '1' );
$form_contents[] = array( 'type'=>'text',  'label' => '住所','name' => 'address','check' => '1' );
$form_contents[] = array( 'type'=>'text',  'label' => 'メールアドレス','name' => 'email','check' => '1,2' );
$form_contents[] = array( 'type'=>'text',  'label' => 'メールアドレス','name' => 'email_confirm','check' => '1,2' );
$form_contents[] = array( 'type'=>'text',  'label' => '電話番号','name' => 'tel','check' => '1,7' );
$form_contents[] = array( 'type'=>'text',  'label' => 'ご家族構成','name' => 'family','check' => '' );


/* ----------------------------------------------------------------------------
ファイルの読み込み
---------------------------------------------------------------------------- */
require_once(dirname(__FILE__)."/class_files/formData.class.php");
require_once(dirname(__FILE__)."/class_files/checkInput.class.php");
require_once(dirname(__FILE__)."/class_files/mailClass.class.php");

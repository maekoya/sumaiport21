'use strict';

/**
 * グローバル変数
 */
var _$window = $(window);
var _ua;

/* --------------------------------------------------------
 GET USER AGENT

smartphone
 * iPhone
 * iPod
 * Windows OS smartphone
 * Android OS smartphone
 * Firefox OS smartphone
 * BlackBerry OS smartphone

tablet
 * iPad
 * Kindle
 * Android OS tablet
 * Windows OS tablet
 * Firefox OS tablet
 * BlackBerry OS tablet
-------------------------------------------------------- */
_ua = (function(u){
  return {
  Tablet:(u.indexOf('windows') && u.indexOf('touch')) || u.indexOf('ipad') || (u.indexOf('android') && u.indexOf('mobile')) || (u.indexOf('firefox') && u.indexOf('tablet')) || u.indexOf('kindle') || u.indexOf('silk') || u.indexOf('playbook'),
  Mobile:(u.indexOf('windows') && u.indexOf('phone')) || u.indexOf('iphone') || u.indexOf('ipod') || (u.indexOf('android') && u.indexOf('mobile')) || (u.indexOf('firefox') && u.indexOf('mobile')) || u.indexOf('blackberry')
  };
})(window.navigator.userAgent.toLowerCase());



/**
 * Common functions
 */
var pageAll = {
  init: function() {
    this.setNavigation();
    this.setSmoothScrollEvent();
    this.setShowPagetopBtn();
    this.setScrollByUrlHash();
    this.setPagerSelectboxEvent();
  },

  setNavigation: function() {
    var $wrapper = $('html');
    var $trigger = $('.js-navigation-btn');
    var $navigation = $('#base-navigation');
    var active_class = 'is-navigation-open';

    $trigger.on('click', function(e){
      e.preventDefault();
      $wrapper.toggleClass(active_class);

      var position_right = $wrapper.hasClass(active_class) ? 0 : -400;
      $navigation.animate({ 'right': position_right }, 400);
    });
  },

  setCurrentNavigationClass: function (){
    var $navigation = $('.js-navigation-list');
    var dir = (location.protocol + '//' + location.host + location.pathname).split('/');
    dir = dir[3];
    $navigation.find('.type-'+ dir ).find('a').addClass('is-active');
  },

  setSmoothScrollEvent: function() {
    var $elem = $('a[href^="#"], a[href^="' + location.pathname + '#"]');
    var eventListener = _ua.Mobile || _ua.Tablet ? 'click' : 'click';

    $elem.on(eventListener,  function() {
      var hash = $(this).attr("href");
      if(hash === "#"){
        return;
      }
      pageAll.smoothScroll(hash);
      return false;
    });
  },

  setScrollByUrlHash: function() {
    _$window.on('load', function(){
      if(location.hash.match(/page/)){
        pageAll.smoothScroll(location.hash);
      }
    });
  },

  setPagerSelectboxEvent: function() {
    var $elem = $('select.js-pager-select');
    $elem.change(function() {
      if ($(this).val() != '') {
        window.location.href = $(this).val();
      }
    });
  },

  /**
   * @param {string} hash スクロール先のエレメントid
   */
  smoothScroll: function(hash) {
    var target = $(hash).offset().top;
    var positionDiffByDvice = _ua.Mobile ? -100 : -100;

    $('body,html').stop().animate({
      scrollTop: target >= 10 ? target + positionDiffByDvice : target
    }, 600, function() {
      $(this).unbind('mousewheel DOMMouseScroll');
    }).bind('mousewheel DOMMouseScroll', function() {
      $(this).queue([]).stop();
      $(this).unbind('mousewheel DOMMouseScroll');
    });

    $('html').removeClass('is-navigation-open')
    $('.js-navgation-btn').removeClass('is-active');
  },

  setShowPagetopBtn: function() {
    $(window).on('scroll', function(){
      var winTop = $(this).scrollTop();
      if(winTop > 300){
        $('#base-page').addClass('is-pagetop-active');
      } else {
        $('#base-page').removeClass('is-pagetop-active');
      }
    });
  },

  /**
   * @param {embedId}   マップを表示するエレメントid
   * @param {lat}       経度
   * @param {lng}       緯度
   * @param {zoomLevel} ズームレベル
   */
  createMap: function(embedId, lat, lng, title, zoomLevel) {
    var mapParams = {
      'embedId': embedId,
      'lat': lat,
      'lng': lng,
      'zoomLevel': zoomLevel
    };
    var latlng = new google.maps.LatLng(mapParams.lat, mapParams.lng);
    var myOptions = {
      zoom: mapParams.zoomLevel,
      center: latlng,
      panControl: true,
      zoomControl: true,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL
      },
      scrollwheel: false,
      draggable: false,
      navigationControl: true,
      mapTypeControl: false,
      streetViewControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById(mapParams.embedId), myOptions);
    /*var icon = {
      url:'/resources/img/icon/icon-googlemaps.2x.png',
      scaledSize:{width: 81, height: 105}
    };*/
    var markerOptions = new google.maps.Marker({
      position: latlng,
      map: map,
      //icon: icon,
      title: mapParams.title
    });
  }
};

/**
 * Page TOP functions
 */
var pageTop = {
  init: function() {

    $(window).on('load', function(){
      resizeWidth();

      setTimeout(function() {
        $('.js-top-cover').addClass('is-animate');
      }, 1000);

      $('.js-top-cover').delay(4000).fadeOut(1000, function(){
        setTimeout(function() {
          go();
        }, 1000);
      });

      var winWidth = _$window.width();

      _$window.on('resize', function (event) {
        var ww = _$window.width();
        if(winWidth == ww) {
          winWidth = ww;
          return false;
        }
        winWidth = ww;
        resizeWidth();
      });

      function resizeWidth(){
        var w = $('.p-top-panel-gellely').width();
        $('.js-top-slide').find('li').css('background-size', w + 'px');
      }

      var $list = $('.js-top-slide');
      var $list_item = $list.find('li');
      var list_item_num = $list_item.length;
      var n = 0;
      var timer;

      function go(){
        timer = setInterval(function(){
          loop();
        }, 4000 );
      }

      function stop(){
         clearInterval(timer);
      }

      /*
      スライドパターン1
      function loop(){
        n = checkNum(n);

        $list_item.eq(n).css({'z-index':'10', 'width':'100%'});
        $list_item.eq( checkNum(n + 1) ).css({'z-index':'9', 'width':'100%'});
        $list_item.eq( checkNum(n + 1) ).addClass('is-pre');

        $list_item.eq(n).animate({'width':0}, 800, function(){
          $list_item.eq(n).css({'z-index':'1'}).removeClass('is-pre');
          n++;
        });
      }*/
      function loop(){
        n = checkNum(n);

        $list_item.eq(n).css({'z-index':'10'});
        $list_item.eq( checkNum(n + 1) ).css({'z-index':'9'}).show();
        $list_item.eq(n).fadeOut(800, function(){
          n++;
        });
      }

      function checkNum(number){
        if( number >= list_item_num){
          number = 0;
        }
        return number;
      }
      $list_item.eq(0).addClass('is-pre').css({'z-index':'10'});
    });

  },

  mainSlide: function() {
    var $slide_list = $('.js-top-slide');
    $slide_list.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: true,
      centerPadding: '0px',
      arrows: false,
      autoplay: true,
      autoplaySpeed: 5000,
      speed: 1000,
      accessibility: false,
      pauseOnHover: false,
      fade: true
    });
  }
};

/**
 * Page About functions
 */
var pageAbout = {
  init: function() {
    this.rentalSlide();
  },

  rentalSlide: function() {
    var $slide_list = $('.js-rental-slide');
    var $slide_contlloler = $(".js-rental-slide-controller");

    $slide_list.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: true,
      centerPadding: '0px',
      arrows: false,
      autoplay: false,
      autoplaySpeed: 5000,
      speed: 300,
      accessibility: false,
      pauseOnHover: false
    });

    $slide_contlloler.on('click', function(e) {
      e.preventDefault();
      if($(this).hasClass('type-next')){
        $slide_list.slick('slickNext');
      }else {
        $slide_list.slick('slickPrev');
      }
    });
  }
};

/**
 * Page CaseStudy functions
 */
var pageCaseStudy = {
  init: function() {
    this.gallerySlide();
  },

  gallerySlide: function() {
    var $slide_list = $('.js-case-study-slide');
    var $slide_contlloler = $(".js-case-study-slide-controller");

    $slide_list.slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      centerMode: true,
      centerPadding: '0px',
      arrows: false,
      autoplay: true,
      autoplaySpeed: 5000,
      speed: 300,
      accessibility: false,
      pauseOnHover: false,
      responsive: [{
          breakpoint: 1800,
          settings: {
            slidesToShow: 3
          }
        }, {
          breakpoint: 801,
          settings: {
            slidesToShow: 2
          }
        }, {
          breakpoint: 481,
          settings: {
            slidesToShow: 1
          }
        }]
    });

    $slide_contlloler.on('click', function(e) {
      e.preventDefault();
      if($(this).hasClass('type-next')){
        $slide_list.slick('slickNext');
      }else {
        $slide_list.slick('slickPrev');
      }
    });
  }
};

/**
 * Page Present functions
 */
var pagePresent = {
  init: function() {
    this.setCatalogueCheckboxValueEvent();
    this.slide();

    window.Parsley.on('form:error', function() {
      pageAll.smoothScroll('#page-form');
    });
  },

  /**
   * 「資料請求ボタン」クリックでフォームのチェックボックスをアクティブに
   */
  setCatalogueCheckboxValueEvent: function() {
    var $setter_btn = $('.js-set-form-catalogue-btn');
    var $chk_items = $('.js-form-catalogue').find('input[type="checkbox"]');

    $setter_btn.on('click', function(e){
      e.preventDefault();

      var target_num = $(this).data('catalogue') - 1;
      $chk_items.eq(target_num).prop('checked', true);
      pageAll.smoothScroll('#page-form');
    });
  },

  slide: function() {
    var $slide_list = $('.js-slide');
    var $slide_contlloler = $(".js-slide-controller");

    $slide_list.slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      centerMode: true,
      centerPadding: '0px',
      arrows: false,
      autoplay: false,
      autoplaySpeed: 5000,
      speed: 300,
      adaptiveHeight: true,
      accessibility: false,
      pauseOnHover: false,
      responsive: [{
          breakpoint: 1800,
          settings: {
            slidesToShow: 3
          }
        }, {
          breakpoint: 801,
          settings: {
            slidesToShow: 2
          }
        }, {
          breakpoint: 481,
          settings: {
            slidesToShow: 1
          }
        }]
    });

    $slide_contlloler.on('click', function(e) {
      e.preventDefault();
      if($(this).hasClass('type-next')){
        $slide_list.slick('slickNext');
      }else {
        $slide_list.slick('slickPrev');
      }
    });
  }
};

/**
 * Page Company functions
 */
var pageCompany = {
  init: function() {
  },

  construction: function() {
    var content = {'type-wood-frame': ''};
    var $frame = '<div class="p-company-item-detail-tag-frame"><span class="type-heading"></span><span class="type-txt"></span></div>';
    var $btn = $('.p-company-item-detail-tag');
    $('#base-page').append($frame);
    $frame = $('.p-company-item-detail-tag-frame');

    var $window = $(window);

    var winWidth = $window.width();

    $window.on('resize', function (event) {
      var ww = $window.width();
      if(winWidth == ww) {
        winWidth = ww;
        return false;
      }
      winWidth = ww;
    });

    $btn.mouseenter(
      function () {
        var pos = $(this).offset();

        if($(this).hasClass('type-wood-frame')){
          $frame.find('.type-heading').text('木造軸組');
          $frame.find('.type-txt').text('柱と梁で構成されているため、大きな開口部など、設計の自由度が高く、増改築の対応にも融通がきき変形敷地や狭小敷地といった、さまざまな敷地条件にも対応できます。');

        } else if($(this).hasClass('type-steel-frame')) {
          $frame.find('.type-heading').text('鉄骨造');
          $frame.find('.type-txt').text('鉄骨部材は工作・加工・防錆塗装までが工場で行われるため、品質が安定しています。また、鉄は柔軟性があり、揺れに対して建物全体がしなるために耐震性が高いのも特徴です。');

        } else if($(this).hasClass('type-log-sets')) {
          $frame.find('.type-heading').text('丸太組構法');
          $frame.find('.type-txt').text('丸太材を水平に積み重ねていき交差する部分はそれぞれ交互に組み合わせていく、ログハウスなどに使われる工法。丸太ではなく角材を使用する場合もあります。');

        } else if($(this).hasClass('type-2x4')) {
          $frame.find('.type-heading').text('2×4');
          $frame.find('.type-txt').text('箱を作るようにパネルを組み上げていく「壁式工法」です。地震、風などの外力を面で受け止め、箱全体に力を分散させるため、耐震性に優れ、断熱性能や気密性能を確保しやすい特徴があります。');

        } else if($(this).hasClass('type-2x6')) {
          $frame.find('.type-heading').text('2×6');
          $frame.find('.type-txt').text('箱を作るようにパネルを組み上げていく「壁式工法」です。地震、風などの外力を面で受け止め、箱全体に力を分散させるため、耐震性に優れ、断熱性能や気密性能を確保しやすい特徴があります。');

        } else if($(this).hasClass('type-importation')) {
          $frame.find('.type-heading').text('輸入住宅');
          $frame.find('.type-txt').text('海外から住宅の建材等を輸入して建てる住宅。外観デザインが輸入先の国の仕様になっているため個性豊かな外観や内装、設備があります。');

        } else if($(this).hasClass('type-wood-panel')) {
          $frame.find('.type-heading').text('木質パネル');
          $frame.find('.type-txt').text('工場で規格化されたパネルを生産し、現場で住宅として組立てる工法。壁で躯体を支え、工場生産率が高いため施工精度が高い、現場作業が少なく工期が短縮できるなどの利点があります。');
        }

        if(winWidth < 800){
          $frame.css({'top': pos.top + 50, 'left': pos.left});
        } else {
          $frame.css({'top': pos.top, 'left': pos.left + 170});
        }
        $frame.addClass('is-active');
      }).mouseleave(function (){
        $frame.removeClass('is-active');
    });
  }
};

/**
 * Page CaseCompetition functions
 */
var pageCaseCompetition = {
  init: function() {
  },

  conpe201507: function() {
    var $frame = '<div class="p-case-competition-conpe-floor-caption"><span class="type-heading">POINT<span class="type-heading-num"></span></span><span class="type-txt"></span></div>';
    var $btn = $('.p-case-competition-conpe-floor-point');
    $('#base-page').append($frame);
    $frame = $('.p-case-competition-conpe-floor-caption');

    var $window = $(window);

    var winWidth = $window.width();

    $window.on('resize', function (event) {
      var ww = $window.width();
      if(winWidth == ww) {
        winWidth = ww;
        return false;
      }
      winWidth = ww;
    });


    $btn.mouseenter( function () {
      var pos = $(this).offset();

      if($(this).hasClass('js-point-1-1')){
        $frame.find('.type-heading-num').text('1');
        $frame.find('.type-txt').text('家事の負担を軽減できるよう、水まわりの動線を工夫しました。キッチンからユーティリティが近いので、お子さんの面倒をみながらお料理して、ちょっとした合間にお洗濯…というのも可能です。');
      }
      if($(this).hasClass('js-point-1-2')){
        $frame.find('.type-heading-num').text('2');
        $frame.find('.type-txt').text('ママの手仕事やお子さんの遊び場に使えるスペースを用意しました。お料理していても目が届く場所なので、安心して台所仕事ができます。オモチャを散らかしてもリビングからは見えないので急なお客様が来ても安心です。');
      }
      if($(this).hasClass('js-point-1-3')){
        $frame.find('.type-heading-num').text('3');
        $frame.find('.type-txt').text('2階のホールにはゆったりとしたフリースペースを。入り口を仕切れば客間にも収納スペースとしても利用が可能。またWCLのスペースを調整して、ご主人の書斎スペースにすることもできます。');
      }

      if($(this).hasClass('js-point-2-1')){
        $frame.find('.type-heading-num').text('1');
        $frame.find('.type-txt').text('玄関からリビングへ抜ける他に、シュークローゼットからユーティリティやキッチンに入れる動線を設けました。重いお買い物の荷物を持って遠回りしてキッチンに入る必要もありません。');
      }
      if($(this).hasClass('js-point-2-2')){
        $frame.find('.type-heading-num').text('2');
        $frame.find('.type-txt').text('1階の洋間は開口部を広く設定しました。お客様がお泊りのときは客間として、それ以外のときは引き込み戸を開け放つことで、リビングと一体となって開放感を演出します。');
      }
      if($(this).hasClass('js-point-2-3')){
        $frame.find('.type-heading-num').text('3');
        $frame.find('.type-txt').text('ご主人が希望されていた書斎スペースはこぢんまりとしていますが、寝室から離す事でご主人の完全なプライベート空間に。WCLのスペースを調整することで、広げることも可能です。');
      }

      if($(this).hasClass('js-point-3-1')){
        $frame.find('.type-heading-num').text('1');
        $frame.find('.type-txt').text('階段を中央に配置したことにより、回遊式の動線を実現しました。奥さまの家事での移動の負担も軽減します。');
      }
      if($(this).hasClass('js-point-3-2')){
        $frame.find('.type-heading-num').text('2');
        $frame.find('.type-txt').text('各部屋にはクローゼット、キッチンにはパントリーなど各部屋に収納スペースを確保。階段下も収納に利用できるので、お子さんの成長に合わせて増えていく物もしっかり収納できます。');
      }
      if($(this).hasClass('js-point-3-3')){
        $frame.find('.type-heading-num').text('3');
        $frame.find('.type-txt').text('階段を建物の中央に配置して、朝から夕方まで日が差し込む明るい家をイメージしました。奥さまがキッチンに居ても、お子さんの「ただいま」はちゃんと分かります。');
      }

      if($(this).hasClass('js-point-4-1')){
        $frame.find('.type-heading-num').text('1');
        $frame.find('.type-txt').text('奥さまの手仕事の場にもなるママスペースに設けた収納や、キッチンのダクトBOXスペースの上に食品庫を備えるなど、空間を立体で考えて収納スペースを作りました。床下も収納に使えますよ。');
      }
      if($(this).hasClass('js-point-4-2')){
        $frame.find('.type-heading-num').text('2');
        $frame.find('.type-txt').text('1階の洋室には3枚引き戸を。すべて外せば、LDKの開放感がグンとアップします。コンパクトでありながら、手狭さを感じさせない間取りです');
      }
      if($(this).hasClass('js-point-4-3')){
        $frame.find('.type-heading-num').text('3');
        $frame.find('.type-txt').text('ご夫婦の寝室にはWCLを設けて、さらにその奥にご主人の書斎をしつらえました。趣味やお仕事で使えるまさに隠れ家的な空間です。');
      }

      if($(this).hasClass('js-point-5-1')){
        $frame.find('.type-heading-num').text('1');
        $frame.find('.type-txt').text('LDKと洋室には一体感を持たせながらも、それぞれに独立した存在感を持たせました。LDK中央の化粧柱は、キッチンからは周囲を見渡せますが、お客様からキッチンを伺えないよう配置してます。');
      }
      if($(this).hasClass('js-point-5-2')){
        $frame.find('.type-heading-num').text('2');
        $frame.find('.type-txt').text('ゆったりとしたスペースを確保した主寝室には3畳の広々としたWCLを設けました。ご主人希望の書斎スペースも主寝室の隣に配置。扉を開ければ閉塞感も感じさせません。');
      }
      if($(this).hasClass('js-point-5-3')){
        $frame.find('.type-heading-num').text('3');
        $frame.find('.type-txt').text('2階には南と西にそれぞれ大きな窓をしつらえたフリースペースを設けました。日当たりがいいので洗濯物を干したり、お子さんの遊び場としてもお使いになれますよ。');
      }

      if(winWidth < 800){
        $frame.css({'top': pos.top + 45, 'left': 10 });
      } else {
        $frame.css({'top': pos.top + 60, 'left': pos.left});
      }
      $frame.addClass('is-active');
    }).mouseleave( function () {
      $frame.removeClass('is-active');
    });
  },

  conpe201510: function() {
    var $frame = '<div class="p-case-competition-conpe-floor-caption"><span class="type-heading">POINT<span class="type-heading-num"></span></span><span class="type-txt"></span></div>';
    var $btn = $('.p-case-competition-conpe-floor-point');
    $('#base-page').append($frame);
    $frame = $('.p-case-competition-conpe-floor-caption');

    var $window = $(window);

    var winWidth = $window.width();

    $window.on('resize', function (event) {
      var ww = $window.width();
      if(winWidth == ww) {
        winWidth = ww;
        return false;
      }
      winWidth = ww;
    });


    $btn.mouseenter( function () {
      var pos = $(this).offset();

      if($(this).hasClass('js-point-1-1')){
        $frame.find('.type-heading-num').text('1');
        $frame.find('.type-txt').text('玄関はシューズクロークからもホールに出入りできる2WAYに。自転車やタイヤなどを収納したいとご希望のあった物置は、風除室を通って物を出し入れできるようにしました。');
      }
      if($(this).hasClass('js-point-1-2')){
        $frame.find('.type-heading-num').text('2');
        $frame.find('.type-txt').text('お子さんが大きくなるにしたがい増えていく物もしっかり収納できるスペースを確保。また食品庫は夏は涼しく、冬は冷えすぎないよう、この部屋独自の断熱も施しています。');
      }
      if($(this).hasClass('js-point-1-3')){
        $frame.find('.type-heading-num').text('3');
        $frame.find('.type-txt').text('ご主人がご希望された造り付けの本棚は主寝室へ。壁一面をブックラックにしました。それでも収納が足りない場合は換気システムのある屋根裏のスペースも利用可能です。');
      }

      if($(this).hasClass('js-point-2-1')){
        $frame.find('.type-heading-num').text('1');
        $frame.find('.type-txt').text('エントランスは玄関ホールとシュークロークの2WAYに。また玄関先で来客の応対をする際にリビングが丸見えにならないよう、玄関ホールからリビングへの動線にはドアを1枚設けました。');
      }
      if($(this).hasClass('js-point-2-2')){
        $frame.find('.type-heading-num').text('2');
        $frame.find('.type-txt').text('南からの光が注ぎ込むLDは、隣接する和室の2枚の引込戸を開けると開放感がアップ。床にはナラの無垢材を用います。身体にやさしく、また時と共に味わいも出てきますよ。');
      }
      if($(this).hasClass('js-point-2-3')){
        $frame.find('.type-heading-num').text('3');
        $frame.find('.type-txt').text('階段を上った2階のホールは5.5帖と贅沢に。奥様の電子ピアノやご主人の希望の本棚を設置し、憩いの場として、また物干しスペースとしても利用できます。高機密住宅は意外と音が反響するのですが、床の防音対策も取ってます。');
      }

      if($(this).hasClass('js-point-3-1')){
        $frame.find('.type-heading-num').text('1');
        $frame.find('.type-txt').text('北西から風が吹く冬場を考慮して玄関ポーチは東側に。エントランスの隣にタイヤも収納できるシューズクロークを置きました。玄関ホールにはニッチを設け、空間にアクセントを付けています。');
      }
      if($(this).hasClass('js-point-3-2')){
        $frame.find('.type-heading-num').text('2');
        $frame.find('.type-txt').text('南側に場所を取ったLDKは約17帖。連結する和室は洋和室風にし、ふすまを開けておいてもLDKとの雰囲気に違和感はありません。ニッチには電子ピアノを置いていただくイメージです。');
      }
      if($(this).hasClass('js-point-3-3')){
        $frame.find('.type-heading-num').text('3');
        $frame.find('.type-txt').text('玄関からの通路と洋和室とを結ぶ隠し部屋的なスペースは「マンガ道」と名付け、ご主人の希望にあった漫画の収納スペースに。1000冊以上収納できます。ゴロっと横になるのも可能ですよ。');
      }

      if($(this).hasClass('js-point-4-1')){
        $frame.find('.type-heading-num').text('1');
        $frame.find('.type-txt').text('玄関の隣にはゆったりとしたシューズクロークを。玄関から伸びる突き当たりのユーティリティには来客の目線を切る吊り戸を設け、奥様目線に立った回遊動線を実現しました。');
      }
      if($(this).hasClass('js-point-4-2')){
        $frame.find('.type-heading-num').text('2');
        $frame.find('.type-txt').text('窓から光がたっぷりと差し込むLDは15.8帖ととっても広々。隣にしつらえたフチ無し畳を配した洋和室はLDとの雰囲気もマッチ。引き戸を開け放てば、さらに開放感がアップします。');
      }
      if($(this).hasClass('js-point-4-3')){
        $frame.find('.type-heading-num').text('3');
        $frame.find('.type-txt').text('2階の小上がりをしつらえたフリースペースを用意。ご主人が大切にしているマンガを並べる本棚を設け、一家団らんのセカンドリビングとして、また物干しスペースとしてもご利用できます。');
      }

      if($(this).hasClass('js-point-5-1')){
        $frame.find('.type-heading-num').text('1');
        $frame.find('.type-txt').text('玄関ポーチからは雨雪にあたらずに物置へのアクセスが可能。テラスへも出入りできます。ホールにはニッチをあしらったエントランスとシューズクロークの2箇所から出入り可能です。');
      }
      if($(this).hasClass('js-point-5-2')){
        $frame.find('.type-heading-num').text('2');
        $frame.find('.type-txt').text('LDKは20帖とかなり広々。キッチンにはカウンターを設けて、LDとの一体感をもたせました。LDKからUT・玄関と結んだ回遊動線も家事には便利かと思います。ご主人の本棚は階段下に。カーテンなどで仕切ればお客様の目線も気になりません。');
      }
      if($(this).hasClass('js-point-5-3')){
        $frame.find('.type-heading-num').text('3');
        $frame.find('.type-txt').text('2階の子ども部屋は将来、2人目がお生まれになったときに仕切る事を考えて一部屋に。仮想地の周囲が住宅密集地という事を考え、リクエストになかった吹抜けと天窓を敢えて設けました。');
      }

      if(winWidth < 800){
        $frame.css({'top': pos.top + 45, 'left': 10 });
      } else {
        $frame.css({'top': pos.top + 60, 'left': pos.left});
      }
      $frame.addClass('is-active');
    }).mouseleave( function () {
      $frame.removeClass('is-active');
    });
  },
  conpe201602: function() {
    var $frame = '<div class="p-case-competition-conpe-floor-caption"><span class="type-heading">POINT<span class="type-heading-num"></span></span><span class="type-txt"></span></div>';
    var $btn = $('.p-case-competition-conpe-floor-point');
    $('#base-page').append($frame);
    $frame = $('.p-case-competition-conpe-floor-caption');

    var $window = $(window);

    var winWidth = $window.width();

    $window.on('resize', function (event) {
      var ww = $window.width();
      if(winWidth == ww) {
        winWidth = ww;
        return false;
      }
      winWidth = ww;
    });


    $btn.mouseenter( function () {
      var pos = $(this).offset();

      if($(this).hasClass('js-point-1-1')){
        $frame.find('.type-heading-num').text('1');
        $frame.find('.type-txt').text('廊下にある洗面台はドアを設けて洗面室と一体化させました。入浴時は洗面室のドアを閉じれば、洗面台を独立して使えます。また洗面室を狭めて隣接する洋室を広げるのも可能です。');
      }
      if($(this).hasClass('js-point-1-2')){
        $frame.find('.type-heading-num').text('2');
        $frame.find('.type-txt').text('2階のキッチンはアイランド式にしました。従来の向きだとお客様がいらしたときに台所の裏の様子が丸見えになるので、動線の方を向くように設置場所も工夫してあります。');
      }
      if($(this).hasClass('js-point-1-3')){
        $frame.find('.type-heading-num').text('3');
        $frame.find('.type-txt').text('廊下にあった洗面台を洗面室の中に移動させました。空いたスペースはクローゼットに変更。お嬢さんたちの成長と共に増えていく物もたっぷり収納できますね。');
      }

      if($(this).hasClass('js-point-2-1')){
        $frame.find('.type-heading-num').text('1');
        $frame.find('.type-txt').text('1階のホールにある空きスペースは観音開きの扉を設けてお洒落なクローゼットに。洗面室の入り口には引き戸をしつらえて、プライバシーもしっかりと保たれます。');
      }
      if($(this).hasClass('js-point-2-2')){
        $frame.find('.type-heading-num').text('2');
        $frame.find('.type-txt').text('2階のLDKにあるスペースにはカウンターを設けてお子様の勉強スペースに。壁には調湿・消臭・抗菌作用のあるエコカラットを配して、空間全体にアクセントを付けています。');
      }
      if($(this).hasClass('js-point-2-3')){
        $frame.find('.type-heading-num').text('3');
        $frame.find('.type-txt').text('2階の玄関を入ったところのホールは廊下を狭めて、洗面室のスペースを広くしました。その分だけ生じる家に入った瞬間の圧迫感は壁を曲線にする事で和らげています。');
      }

      if($(this).hasClass('js-point-3-1')){
        $frame.find('.type-heading-num').text('1');
        $frame.find('.type-txt').text('1階の和室はご希望通りのフローリングに。床のフローリングには各フロア共に無垢材を提案しました。木の持つ温かみがあるほか、長期的に見ればメンテナンスも楽になります。');
      }
      if($(this).hasClass('js-point-3-2')){
        $frame.find('.type-heading-num').text('2');
        $frame.find('.type-txt').text('2階のLDKにはアイランド式のキッチンをご家族が冷蔵庫への行き来がスムーズになるように設けました。また壁の窪みにはちょっとした作業に使えるカウンターもあしらいます。');
      }
      if($(this).hasClass('js-point-3-3')){
        $frame.find('.type-heading-num').text('3');
        $frame.find('.type-txt').text('お嬢さんが大きくなった時のことを見据えて洗面室と脱衣所は分離。朝の支度時に一人が脱衣所を使っていると他の人は歯を磨けない…なんて事もありません。また廊下を狭めた分、脱衣所を広げております。');
      }

      if(winWidth < 800){
        $frame.css({'top': pos.top + 45, 'left': 10 });
      } else {
        $frame.css({'top': pos.top + 60, 'left': pos.left});
      }
      $frame.addClass('is-active');
    }).mouseleave( function () {
      $frame.removeClass('is-active');
    });
  }
};



$(function(){
  pageAll.init();
});

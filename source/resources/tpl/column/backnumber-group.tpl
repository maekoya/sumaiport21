<?php
/**
 * isBacknumberSelectActive
 *
 * @param {string}
 * @param {string}
 */
function isBacknumberSelectActive($current_date, $this_date){
  if($current_date != $this_date){
    return false;
  }
  echo ' selected';
}
?>

  <?php if(!empty($backnumber)): ?>
  <div class="p-backnumber-group type-green">
    <div class="p-backnumber">
      <div class="p-backnumber-inner">
        <p class="p-backnumber-label"><img src="/resources/img/text/txt-backnumber-2.png" alt="バックナンバー"></p>
        <p class="p-backnumber-txt-1">これまでの更新を見る</p>
        <div class="p-backnumber-select">
          <select class="c-form-select js-pager-select">
            <option value="/column/201602/"<?php isBacknumberSelectActive($backnumber['date'], '201602'); ?>>2016年02月</option>
            <option value="/column/201510/"<?php isBacknumberSelectActive($backnumber['date'], '201510'); ?>>2015年10月</option>
            <option value="/column/201507/"<?php isBacknumberSelectActive($backnumber['date'], '201507'); ?>>2015年07月</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>


    <div id="base-navigation">
        <div class="p-navigation-btn js-navigation-btn"></div>
        <div class="p-navigation">
            <p class="p-navigation-logo"><a href="/"><img class="p-header-logo-1" src="/resources/img/base/logo-sumai-lead.svg" width="232" alt="北海道の住まいづくり　すまいポート21"></a></p>
            <ul class="p-navigation-menu-list">
                <li class="p-navigation-menu-list-item"><a href="/">トップページ</a></li>
                <li class="p-navigation-menu-list-item"><a href="/competition/">設計コンペとは？</a></li>
                <li class="p-navigation-menu-list-item"><a href="/case-competition/">設計コンペで家を建てよう！</a></li>
                <li class="p-navigation-menu-list-item"><a href="/case-study/">みんなの施工事例</a></li>
                <li class="p-navigation-menu-list-item"><a href="/company/">掲載企業</a></li>
                <li class="p-navigation-menu-list-item"><a href="/about/">すまいポート21について</a></li>
                <li class="p-navigation-menu-list-item"><a href="/couleur/">ショールーム体験レポート</a></li>
                <!--<li class="p-navigation-menu-list-item"><span>今月の赤ちゃん（coming soon）</span></li>-->
                <li class="p-navigation-menu-list-item"><a href="/present/">一押し！商品紹介</a></li>
                <li class="p-navigation-menu-list-item"><a href="http://www.sumai21.net/area/hokkaido/sumai21-sapporo-ch/event.php" target="_blank">セミナー情報（外部リンク）</a></li>
                <li class="p-navigation-menu-list-item"><a href="/column/">初めての住まいづくりコラム</a></li>
            </ul>
            <ul class="p-navigation-btn-list">
                <li class="p-navigation-btn-list-item"><a href="/reservation/">来場予約</a></li>
            </ul>
        </div>
    </div><!-- /#base-navigation-->
    <div id="base-navigation-frame" class="js-navigation-btn"></div>

<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="UTF-8">
<title><?php echo $page['title']; ?></title>
<meta name="description" content="<?php echo $page['discription']; ?>">
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
<meta name="format-detection" content="telephone=no,address=no,email=no">
<meta name="google-site-verification" content="">
<meta name="author" content="Sumai Port21®">
<meta name="copyright" content="Copyright©2015 Sumai Port21®">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=Edge, chrome=1">
<meta http-equiv="imagetoolbar" content="no">
<![endif]-->

<!-- Ogp -->
<meta property="og:image" content="https://sumai21-hokkaido.com/resources/ogp/ogp_image.jpg" />

<!-- Favicon -->
<link rel="shortcut icon" href="/resources/favicon/favicon.ico">

<!-- StyleSheet -->
<link rel="stylesheet" href="/resources/css/common.css?v=10">
<link rel="stylesheet" href="/resources/css/append.css">

<!-- Javascript -->
<script src="//maps.google.com/maps/api/js?sensor=false"></script>
<script src="/resources/js/lib.min.js?v=10"></script>
<script src="/resources/js/script.min.js?v=10"></script>
<!--[if lt IE 9]>
<script src="/resources/js/html5shiv-printshiv.js"></script>
<![endif]-->

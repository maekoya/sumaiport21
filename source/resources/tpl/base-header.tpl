
    <header id="base-header">
        <div class="p-header">
            <p class="p-header-logo-group">
                <a href="/"><img class="p-header-logo-1" src="/resources/img/base/logo-sumai-lead.svg" alt="北海道の住まいづくり　すまいポート21">
                <img class="p-header-logo-2" src="/resources/img/base/logo-sumaiport21.svg" alt="住まいのサポートセンター すまいポート21" width="154"></a>
            </p>
            <ul class="p-header-num-list">
                <li class="p-header-num-list-item type-build"><a href="/company/"><span class="type-label">建築</span><span class="type-num">43</span>社</a></li>
                <li class="p-header-num-list-item type-reform"><a href="/company/reform/"><span class="type-label">リフォーム</span><span class="type-num">12</span>社</a></li>
                <li class="p-header-num-list-item type-outdoor"><a href="/company/outdoor/"><span class="type-label">外構</span><span class="type-num">4</span>社</a></li>
            </ul>
            <ul class="p-header-btn-list">
                <li class="p-header-btn-list-item type-fb"><a href="https://www.facebook.com/sumai21.sapporo.higashi" target="_blank">Facebook</a></li>
                <li class="p-header-btn-list-item type-reservation"><a href="/reservation/">来場予約</a></li>
                <li class="p-header-btn-list-item type-menu"><a href="#" class="js-navigation-btn">MENU</a></li>
            </ul>
            <p class="p-header-reservation-btn"><a href="/present/"><img src="/resources/img/base/header/header-reservation-btn.png" alt="建築会社カタログプレゼント"></a></p>
        </div>
    </header><!-- /#base-header-->

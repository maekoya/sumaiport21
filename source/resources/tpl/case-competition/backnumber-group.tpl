<?php
/**
 * isBacknumberThumbActive
 *
 * @param {number}
 * @param {number}
 */
function isBacknumberThumbActive($current_case, $this_case){
    if($current_case != $this_case){
        return false;
    }
    echo 'active';
}

/**
 * isBacknumberSelectActive
 *
 * @param {string}
 * @param {string}
 */
function isBacknumberSelectActive($current_date, $this_date){
    if($current_date != $this_date){
        return false;
    }
    echo ' selected';
}
?>

        <?php if(!empty($backnumber)): ?>
        <div class="p-backnumber-group">
            <div class="p-backnumber">
                <div class="p-backnumber-inner">
                    <p class="p-backnumber-label"><img src="/resources/img/text/txt-backnumber-1.png" alt="バックナンバー"></p>
                    <p class="p-backnumber-txt-1">これまでの更新を見る</p>
                    <div class="p-backnumber-select">
                        <select class="c-form-select js-pager-select">
                            <option value="/case-competition/201602/"<?php isBacknumberSelectActive($backnumber['date'], '201602'); ?>>2016年02月</option>
                            <option value="/case-competition/201510/"<?php isBacknumberSelectActive($backnumber['date'], '201510'); ?>>2015年10月</option>
                            <option value="/case-competition/201507/"<?php isBacknumberSelectActive($backnumber['date'], '201507'); ?>>2015年7月</option>
                        </select>
                    </div>
                    <ul class="p-backnumber-thumb-list">
                        <?php if($backnumber['date'] == '201602'): ?>
                            <li class="p-backnumber-thumb-list-item <?php isBacknumberThumbActive($backnumber['case'], 1); ?>"><a href="/case-competition/201602/"><img src="/case-competition/201602/img/case-1/thumb-1.jpg" alt="2015年10月"></a></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                        <?php endif; ?>
                        <?php if($backnumber['date'] == '201510'): ?>
                            <li class="p-backnumber-thumb-list-item <?php isBacknumberThumbActive($backnumber['case'], 1); ?>"><a href="/case-competition/201510/"><img src="/case-competition/201510/img/case-1/thumb-1.jpg" alt="2015年10月"></a></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                        <?php endif; ?>
                        <?php if($backnumber['date'] == '201507'): ?>
                            <li class="p-backnumber-thumb-list-item <?php isBacknumberThumbActive($backnumber['case'], 2); ?>"><a href="/case-competition/201507/"><img src="/case-competition/201507/img/case-1/thumb-1.jpg" alt="2015年07月"></a></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                            <li class="p-backnumber-thumb-list-item"></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php endif; ?>

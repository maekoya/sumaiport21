
        <div class="p-company-search-group">
            <p class="p-company-search-label">会社名で探す</p>
            <ul class="p-company-search-list">
                <li class="p-company-search-list-item<?php if($const_page == "list-0") { echo " is-active"; } ?>"><a href="/company/">一覧</a></li>
                <li class="p-company-search-list-item<?php if($const_page == "list-1") { echo " is-active"; } ?>"><a href="/company/construction/list-1.html">あ～お</a></li>
                <li class="p-company-search-list-item<?php if($const_page == "list-2") { echo " is-active"; } ?>"><a href="/company/construction/list-2.html">か～こ</a></li>
                <li class="p-company-search-list-item<?php if($const_page == "list-3") { echo " is-active"; } ?>"><a href="/company/construction/list-3.html">さ～そ</a></li>
                <li class="p-company-search-list-item<?php if($const_page == "list-4") { echo " is-active"; } ?>"><a href="/company/construction/list-4.html">た～と</a></li>
                <li class="p-company-search-list-item<?php if($const_page == "list-5") { echo " is-active"; } ?>"><a href="/company/construction/list-5.html">な～の</a></li>
                <li class="p-company-search-list-item<?php if($const_page == "list-6") { echo " is-active"; } ?>"><a href="/company/construction/list-6.html">は～ほ</a></li>
                <li class="p-company-search-list-item<?php if($const_page == "list-7") { echo " is-active"; } ?>"><a href="/company/construction/list-7.html">ま～も</a></li>
                <li class="p-company-search-list-item<?php if($const_page == "list-8") { echo " is-active"; } ?>"><a href="/company/construction/list-8.html">や～よ</a></li>
                <li class="p-company-search-list-item<?php if($const_page == "list-9") { echo " is-active"; } ?>"><a href="/company/construction/list-9.html">ら～ろ</a></li>
                <li class="p-company-search-list-item<?php if($const_page == "list-10") { echo " is-active"; } ?>"><a href="/company/construction/list-10.html">わ</a></li>
            </ul>
        </div>

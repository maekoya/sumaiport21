<?php
session_start();
session_regenerate_id();
ini_set( 'display_errors', 0 );
/* ----------------------------------------------------------------------------
各種設定
---------------------------------------------------------------------------- */
// ワンタイムパスワード発行用キー
$oneTimePassKey = "4qpqlc9jvLopY0O2x9QieWfnH9";

// SMTPサーバ（お名前.comサーバの場合に必要）
//	$smtpServer = "smtp.domain.tld";

/* ----------------------------------------------------------------------------
ページの設定
---------------------------------------------------------------------------- */
// インデックスページ
$topPage = "/";
// インデックスページ
$indexPage = "/present/";
// チェックページ
$checkPage = "/present/";
// 確認ページ
$confirmPage = "/present/confirm.html";
// 完了ページ
$endPage =  "/present/thanks.html#page-form";


/* ----------------------------------------------------------------------------
フォームの設定
---------------------------------------------------------------------------- */
// 戻るボタン
$returnBtn = "戻る";

/* ----------------------------------------------------------------------------
メールの設定 - 管理者メール -
---------------------------------------------------------------------------- */
// 宛先
$mailToAdmin = "sapporo-ch@sumai21.net,sapporo-h@sumai21.net";
$mailToCc = "";
$mailToBcc = "wataru@gear8.com";

// 件名
$mailSubjectAdmin = "カタログプレゼントフォームからお申し込みを受け付けました";

//本文
$mailBodyAdmin = <<<EOD
カタログプレゼントフォームからお申し込みを受け付けました
下記内容を確認の上、担当者は折り返しご連絡ください。

投 稿 者 名 ： {name}
投 稿 時 間 ： {nowDate}
ＩＰアドレス： {ip_address}
ホ ス ト 名 ： {host_name}

--------------------------------------------------
ご入力いただいた情報
受付時間：{nowDate}
--------------------------------------------------

【カタログ】
{catalogue}

【お名前】
{name}

【住所】
{zip}
{pref}{address}

【E-mail】
{email}

【電話番号】
{tel}

【すまいポート21情報誌エブリーの購読希望】
{subscription}

【すまいポート21のご利用を希望されますか？】
{use_ops}

--------------------------------------------------

このメールに心当たりの無い場合は、お手数ですが
下記連絡先までお問い合わせください。

--------------------------------------------------
すまいポート21　札幌東
〒004-0052
札幌市厚別区厚別中央2条1丁目２-２０
営業時間 AM10:00〜PM7:00（水曜日定休）
TEL／011-887-9191
--------------------------------------------------
すまいポート21　札幌中央
〒064-0953
札幌市中央区宮の森3条1丁目４−１タイびる1F
営業時間 AM10:00〜PM7:00（水曜日定休）
TEL／011-590-0108
--------------------------------------------------

北海道の住まいづくり　すまいポート21
Website ： https://sumai21-hokkaido.com/
EOD;


/* ----------------------------------------------------------------------------
メールの設定 - 訪問者 -
---------------------------------------------------------------------------- */
// 差出人名文字化け対策
mb_language('Japanese');
mb_internal_encoding("UTF-8");

// 送信元
$mailFromLabel = "すまいポート21";	 // FROMの名前
$mailFrom = "sapporo-ch@sumai21.net"; // FROMのメールアドレス
$mailFromUser = mb_encode_mimeheader($mailFromLabel)."<" .$mailFrom.">";

// 件名
$mailSubjectUser = "カタログプレゼントフォームからのお申し込みありがとうございます";

// 本文
$mailBodyUser = <<<EOD
{name} 様

この度は、ご予約フォームからお申し込み頂き誠にありがとうございました。
改めて担当者よりご連絡をさせていただきますので、
今しばらくお待ちくださいませ。

--------------------------------------------------
ご入力いただいた情報
受付時間：{nowDate}
--------------------------------------------------

【カタログ】
{catalogue}

【お名前】
{name}

【住所】
{zip}
{pref}{address}

【E-mail】
{email}

【電話番号】
{tel}

【すまいポート21情報誌エブリーの購読希望】
{subscription}

【すまいポート21のご利用を希望されますか？】
{use_ops}

--------------------------------------------------

このメールに心当たりの無い場合は、お手数ですが
下記連絡先までお問い合わせください。

--------------------------------------------------
すまいポート21　札幌東
〒004-0052
札幌市厚別区厚別中央2条1丁目２-２０
営業時間 AM10:00〜PM7:00（水曜日定休）
TEL／011-887-9191
--------------------------------------------------
すまいポート21　札幌中央
〒064-0953
札幌市中央区宮の森3条1丁目４−１タイびる1F
営業時間 AM10:00〜PM7:00（水曜日定休）
TEL／011-590-0108
--------------------------------------------------

北海道の住まいづくり　すまいポート21
Website ： https://sumai21-hokkaido.com/
EOD;


/* ----------------------------------------------------------------------------
フォームコンテンツの設定
---------------------------------------------------------------------------- */
// $form_contents[] = array( 'label' => '', 'type' => '', 'name' => '', 'check' => '' );
//
// ■ label ： 表示名
//
// ■ type ： inputのタイプ
//
//            text: テキスト
//            radio: ラジオボタン
//            checkbox: チェックボックス
//            select: セレクトボックス
//            hidden: 隠し要素
//
// ■ name  ： inputのname属性に指定する文字列
//
// ■ check ： エラーチェックをする項目の設定。複数設定する場合は「,」区切りで指定する。
//
//             1: 空白チェック
//             2: メールアドレスチェック
//             3: 半角英数チェック
//             4: 半角数字チェック
//             5: 日付チェック ×
//             6: 郵便番号チェック
//             7: 電話番号チェック(044-555-5555(ハイフン入力必須) もしくは [　　　] - [　　　] - [　　　](フォームを3つに分割)
//             8: URLチェック
//             9: 都道府県チェック
//             10: カタカナチェック
//
// ----------------------------------------------------------------------------

// chekcboxを配列で設定すると空白チェック=1が使えないみたいです

$form_contents = array();
$form_contents[] = array( 'type'=>'checkbox',  'label' => 'カタログ','name' => 'catalogue','check' => '' );
$form_contents[] = array( 'type'=>'text',  'label' => 'お名前','name' => 'name','check' => '1' );
$form_contents[] = array( 'type'=>'text',  'label' => '郵便番号','name' => 'zip','check' => '' );
$form_contents[] = array( 'type'=>'select',  'label' => '住所','name' => 'pref','check' => '1' );
$form_contents[] = array( 'type'=>'text',  'label' => '住所','name' => 'address','check' => '1' );
$form_contents[] = array( 'type'=>'text',  'label' => 'メールアドレス','name' => 'email','check' => '1,2' );
$form_contents[] = array( 'type'=>'text',  'label' => 'メールアドレス','name' => 'emailconf','check' => '1,2' );
$form_contents[] = array( 'type'=>'text',  'label' => '電話番号','name' => 'tel','check' => '1,7' );
$form_contents[] = array( 'type'=>'radio',  'label' => 'すまいポート21情報誌エブリーの購読希望','name' => 'subscription','check' => '' );
$form_contents[] = array( 'type'=>'checkbox',  'label' => 'すまいポート21のご利用を希望されますか？','name' => 'use_ops','check' => '' );

/* ----------------------------------------------------------------------------
ファイルの読み込み
---------------------------------------------------------------------------- */
require_once(dirname(__FILE__)."/class_files/formData.class.php");
require_once(dirname(__FILE__)."/class_files/checkInput.class.php");
require_once(dirname(__FILE__)."/class_files/mailClass.class.php");

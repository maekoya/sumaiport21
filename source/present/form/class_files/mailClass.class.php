<?php

/*

メソッド一覧

MailClass()
	引数：なし
	返値：なし
	コンストラクタ。各変数の初期化。

SetSubject()
	引数：(1)メールの件名
	返値：なし
	メールタイトルの設定。

SetBody()
	引数：(1)メールの本文
	返値：なし
	メールタイトルの設定。

SetTo()
	引数：(1)メールの宛先アドレス
	返値：なし
	メールタイトルの設定。

SetFrom()
	引数：(1)メールの差出人アドレス
	返値：なし
	メールタイトルの設定。

 */

class mailClass {

	var $subject;
	var $to;
	var $cc;
	var $bcc;
	var $from;
	var $body;

    function mailClass() {

		$this->subject ="";
		$this->to ="";
		$this->from ="";
		$this->cc ="";
		$this->bcc ="";
		$this->body ="";

	}

	function setMail( $data1, $data2, $data3, $data4, $data5, $data6) {

		$this->SetSubject($data1);
		$this->SetBody($data2);
		$this->SetFrom($data3);
		$this->SetTo($data4);
		$this->SetCc($data5);
		$this->SetBcc($data6);

	}

	function setSubject($data) {

		$this->subject = $data;

	}

	function setBody($data) {

		$this->body = $data;

	}

	function setFrom($data) {

		$this->from = $data;

	}

	function setTo($data) {

		$this->to = $data;

	}

	function setCc($data) {

		$this->cc = $data;

	}

	function setBcc($data) {

		$this->bcc = $data;

	}

	function sendMail(){

		global $smtpServer;
		if (isset($smtpServer) && !empty($smtpServer))
		{
			// お名前.comサーバで必要な設定
			ini_set("SMTP", $smtpServer);
			ini_set("sendmail_from", $this->from);
		}

		mb_language('Japanese');
		mb_internal_encoding("UTF-8");

		mb_send_mail( $this->to, $this->subject, $this->body, "From:".$this->from."\nBcc:".$this->bcc."\nCc:".$this->cc );
	}

	function printMail(){

		echo "件名：".$this->subject."<br />";
		echo "内容：".nl2br($this->body)."<br />";
		echo "差出人：".$this->from."<br />";
		echo "宛先：".$this->to;
		echo "Cc：".$this->cc;
		echo "Bcc：".$this->bcc;

	}

}

?>
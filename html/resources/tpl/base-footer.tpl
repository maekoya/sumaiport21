
    <footer id="base-footer">
        <div class="p-footer">
            <div class="p-footer-contact-group">
                <div class="p-footer-contact-inner">
                    <img src="/resources/img/base/footer/footer-contact-txt-1.png" alt="住まいづくり、もっと楽しく、もっと自分らしく。">
                    <p><a href="/reservation/">メールでのお問い合わせ</a></p>
                </div>
            </div>

            <div class="p-footer-address-group">
                <div class="p-footer-address-item">
                    <div class="p-footer-address-item-image"><img src="/resources/img/base/footer/footer-image-higashi-sapporo.jpg" alt="すまいポート21札幌東"></div>
                    <p class="p-footer-address-item-label"><img src="/resources/img/base/logo-sumaiport21-higashi-sapporo.svg" alt="すまいポート21札幌東"></p>
                    <p class="p-footer-address-item-tel"><span class="type-label">TEL：</span><span class="type-num">011-887-9191</span></p>
                    <p class="p-footer-address-item-detail">〒004-0052<br>
                    札幌市厚別区厚別中央2条1丁目２-２０<br>
                    営業時間 AM10:00〜PM7:00（水曜日定休）</p>
                </div>
                <div class="p-footer-address-item">
                    <div class="p-footer-address-item-image"><img src="/resources/img/base/footer/footer-image-sapporo-chuo.jpg" alt="すまいポート21札幌中央"></div>
                    <p class="p-footer-address-item-label"><img src="/resources/img/base/logo-sumaiport21-sapporo-chuo.svg" alt="すまいポート21札幌中央"></p>
                    <p class="p-footer-address-item-tel"><span class="type-label">TEL：</span><span class="type-num">011-590-0108</span></p>
                    <p class="p-footer-address-item-detail">〒064-0953<br>
                    札幌市中央区宮の森3条1丁目４−１タイびる1F<br>
                    営業時間 AM10:00〜PM7:00（水曜日定休）</p>
                </div>
            </div>

            <hr class="p-footer-line">

            <div class="p-footer-navi-group">
                <ul class="p-footer-navi-list">
                    <li class="p-footer-navi-list-item"><a href="/">トップページ</a></li>
                    <li class="p-footer-navi-list-item"><a href="/competition/">設計コンペとは？</a></li>
                    <li class="p-footer-navi-list-item"><a href="/case-competition/">設計コンペで家を建てよう！</a></li>
                    <li class="p-footer-navi-list-item"><a href="/case-study/">私たちはココで選びました！</a></li>
                    <li class="p-footer-navi-list-item"><a href="/company/">掲載企業</a></li>
                    <li class="p-footer-navi-list-item"><a href="/about/">すまいポート21について</a></li>
                    <li class="p-footer-navi-list-item"><a href="/couleur/">クルールタイアップ企画</a></li>
                    <!--<li class="p-footer-navi-list-item"><a href="#">今月の赤ちゃん</a></li>-->
                    <li class="p-footer-navi-list-item"><a href="/reservation/">来場予約</a></li>
                    <li class="p-footer-navi-list-item"><a href="http://www.sumai21.net/area/hokkaido/sumai21-sapporo-ch/event.php" target="_blank">セミナー情報</a></li>
                    <li class="p-footer-navi-list-item"><a href="/column/">初めての住まいづくりコラム</a></li>
                </ul>
                <p class="p-footer-navi-fb"><a href="https://www.facebook.com/sumai21.sapporo.higashi" target="_blank">Facebookページ</a></p>
            </div>

            <hr class="p-footer-line">

            <div class="p-footer-banner-group">
                <ul class="p-footer-banner-list">
                    <li class="p-footer-banner-list-item"><a href="http://yes1-hokkaido.com/" target="_blank"><img src="/resources/img/base/footer/footer-banner-ie-station.png" alt="イエステーション"></a></li>
                    <li class="p-footer-banner-list-item"><a href="http://www.yesrehome.co.jp/" target="_blank"><img src="/resources/img/base/footer/footer-banner-yes-reform.png" alt="イエスリフォーム"></a></li>
                    <li class="p-footer-banner-list-item"><a href="http://www.ieie.co.jp/" target="_blank"><img src="/resources/img/base/footer/footer-banner-ieie-park.png" alt="イエイエパーク"></a></li>
                </ul>
            </div>

            <hr class="p-footer-line">

            <div class="p-footer-copyright-group">
                <p class="p-footer-copyright-heading"><a href="/"><img src="/resources/img/base/logo-sumaiport21.svg" alt="住まいのサポートセンター すまいポート21"></a></p>
                <p class="p-footer-copyright-txt">Copyright©2015 Sumai Port21®.<br>Aregistered trademark of SumaiPort21. All rights reserved.</p>
            </div>
        </div>
        <div class="p-footer-pagetop-btn"><a href="#base-page"></a></div>
    </footer><!-- /#base-footer -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68130519-1', 'auto');
  ga('send', 'pageview');
</script>
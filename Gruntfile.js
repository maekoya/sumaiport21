module.exports = function(grunt) {
  'use strict';

  require('jit-grunt')(grunt, {
    sprite: 'grunt-spritesmith',
  });
  require('time-grunt')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    clean: {
      dev: [
        'html/**/*'
      ],
      css: [
        'html/resources/css/*'
      ],
      js: [
        'html/resources/js/*'
      ],
      metalsmith: [
        'html/_layouts',
        'html/resources/_scss',
        'html/resources/_fonts',
        'html/resources/js/_script.js'
      ]
    },

    copy: {
      css: {
        files: [
          {
            expand: true,
            cwd: 'source/resources/css/',
            src: ['**'],
            dest: 'html/resources/css/'
          }
        ]
      },
      js: {
        files: [
          {
            expand: true,
            cwd: 'source/resources/js/',
            src: [
              '**',
              '!_script.js',
              '!libs/**'
            ],
            dest: 'html/resources/js/'
          }
        ]
      }
    },

    exec: {
      metalsmith: {
        //command: 'node node_modules/.bin/metalsmith'  //mac環境
        command: 'metalsmith'  //win環境 : グローバルにmetalsmithをインストール
      }
    },

    jshint: {
      options: {
        jshintrc: true
      },
      all: ['source/resources/js/_script.js']
    },

    concat: {
      dev: {
        src: [
          'source/resources/js/libs/jquery-1.11.1.min.js',
          //'source/resources/js/libs/breakpoint.js',
          'source/resources/js/libs/jquery.heightLine.js',
          //'source/resources/js/libs/masonry.pkgd.min.js',
          'source/resources/js/libs/jquery.slick.min.js',
          //'source/resources/js/libs/jquery.magnific-popup.js',
          'source/resources/js/libs/parsley.js'
          //'source/resources/js/libs/jquery.japan-map.min.js'

        ],
        dest: 'source/resources/js/lib.js'
      }
    },

    uglify: {
      dev: {
        files: {
          'source/resources/js/lib.min.js': 'source/resources/js/lib.js',
          'source/resources/js/script.min.js': 'source/resources/js/_script.js'
        }
      }
    },

    sprite: {
      icon: {
        src: 'source/resources/img/_sprite/*.png',
        dest: 'source/resources/img/sprite.png',
        imgPath: '/resources/img/sprite.png',
        retinaSrcFilter: ['source/resources/img/_sprite/*-2x.png'],
        retinaDest: 'source/resources/img/sprite.retina-2x.png',
        retinaImgPath: '/resources/img/sprite.retina-2x.png',
        destCss: 'source/resources/_scss/setting/_sprite.scss',
        padding: 10,
        cssVarMap: function(sprite){
          sprite.name = 'sprite-' + sprite.name;
        }
      }
    },

    sass: {
      dev: {
        cwd: 'source/resources/_scss/',
        src: '*.scss',
        dest: 'source/resources/css',
        expand: true,
        ext: '.css'
      }
    },

    cssbeautifier : {
      files : ['source/resources/css/*.css']
    },

    csscomb: {
      options: {
        config: '.csscomb.json'
      },
      dev: {
        expand: true,
        flatten: true,
        src: 'source/resources/css/*.css',
        dest: 'source/resources/css/'
      }
    },

    autoprefixer: {
      options: {
        browsers: ['ie >= 8', 'ios >= 6', 'android >= 2.3']
      },
      dev: {
        expand: true,
        flatten: true,
        src: 'source/resources/css/*.css',
        dest: 'source/resources/css/'
      }
    },

    webfont: {
        icons: {
            src: 'source/resources/_fonts/svg/*.svg',
            dest: 'source/resources/fonts',
            destCss: 'source/resources/_scss/setting/',
            options: {
              font: 'customfont',
              stylesheet: 'scss',
              engine: 'node',
              hashes: false,
              template: 'source/resources/_fonts/tmpl.css',
              destHtml: 'source/_DEVELOP/',
              htmlDemoTemplate: 'source/resources/_fonts/tmpl.html',
              relativeFontPath: '../fonts/'
            }
        }
    },

    watch: {
      html: {
        files: ['source/**/*.html', 'source/**/*.tpl'],
        tasks: [/*'clean:dev', 'exec', 'clean:metalsmith'*/]
      },
      sass: {
        files: ['source/resources/_scss/**/*.scss'],
        tasks: ['clean:css', 'sass', 'cssbeautifier', 'csscomb', 'autoprefixer', 'copy:css']
      },
      js: {
        files: ['source/resources/js/_script.js'],
        tasks: [/*'jshint', */'concat', 'uglify', 'clean:js', 'copy:js']
      },
    }
  });

  grunt.registerTask('server', [/*'clean:dev', */'sass', 'sprite', 'cssbeautifier', 'csscomb', 'autoprefixer', /*'exec:metalsmith', 'clean:metalsmith',*/ 'watch']);
  grunt.registerTask('default', [/*'clean:dev',*/ 'sass', 'sprite', 'cssbeautifier', 'csscomb', 'autoprefixer', /*'exec:metalsmith', 'clean:metalsmith'*/]);
};
